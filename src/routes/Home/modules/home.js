import update from "react-addons-update";
import constants from "./actionConstants";
import { Dimensions } from "react-native";
import distance from '../../../utils/calculator.js';
import axios from 'axios'
const {
    GET_CURRENT_LOCATION,
    GET_LIST_POKEMON
}=constants;
const { width, height } = Dimensions.get("window");

const ASPECT_RATIO = width / height;

const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA= ASPECT_RATIO = LATITUDE_DELTA;


//Actions
export function getCurrentLocation(){
  return(dispatch)=>{
    navigator.geolocation.getCurrentPosition(
      (position)=>{
        dispatch({
          type: GET_CURRENT_LOCATION,
          payload:position
        });
      },
      (error)=> console.log(error.message),
    { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
  }
}
export function getListPokemon(){
  return(dispatch, store)=>{
    axios.get('https://dev.api.vivezclichy.fr/assets/pokemons.json')
    .then((response)=>{
      var ListPokemonFiltred=[];
      console.log(response.data)
      for (let pokemon of response.data) {
          console.log(pokemon.name);
          const positionPokemon={
            latitude:pokemon.latitude,
            longitude: pokemon.longitude
          }
            console.log(positionPokemon);

              console.log(store().home.region);
            const dist = distance(store().home.region,positionPokemon);
              console.log(dist);
              pokemon.distance = dist;
              if(dist<500){
          ListPokemonFiltred.push(pokemon);
              }

      }
      dispatch({
        type: GET_LIST_POKEMON,
        payload:ListPokemonFiltred
      });

    })
   .catch((err)=>{
     console.log(err.message);
   })
  }
}

//Actions Handlers

function handleGetCurrentLocation(state, action){
  return update(state, {
    region: {
      latitude:{
        $set: action.payload.coords.latitude
      },
      longitude: {
        $set: action.payload.coords.longitude
      },
      latitudeDelta: {
        $set: LATITUDE_DELTA
      },
      longitudeDelta: {
        $set: LONGITUDE_DELTA
      }


    }
  })
}
function handleGetListPokemon(state, action){
  return update(state, {
    listPokemon:{
      $set:action.payload
    }
  })
}

const ACTION_HANDLERS= {
  GET_CURRENT_LOCATION: handleGetCurrentLocation,
  GET_LIST_POKEMON: handleGetListPokemon
}
const initialState ={
  region: {
    latitude: 3.146642,
    longitude:101.695845,
    latitudeDelta:0.0922,
    longitudeDelta: 0.0421
  },
  listPokemon:[]
};

export function HomeReducer(state = initialState, action){
  const handler = ACTION_HANDLERS[action.type];
return handler ? handler(state, action) : state;
}
