import { connect } from "react-redux";
import Home from "../components/Home";
import { getCurrentLocation, getListPokemon} from "../modules/home";

const mapStateToProps = (state) =>({
  region: state.home.region,
  listPokemon: state.home.listPokemon || []
});

const mapActionCreators = {
  getCurrentLocation,
  getListPokemon
};

export default connect (mapStateToProps,mapActionCreators)(Home)
