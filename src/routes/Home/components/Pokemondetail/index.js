import React, {Component} from 'react'
import {Image, StyleSheet, Text, View,Dimensions} from 'react-native'
import IconEntypo from 'react-native-vector-icons/Entypo'
const { width, height } = Dimensions.get("window");
export const Pokemondetail =({name, image, abilities, weakness,weight, types, height, distance}) => {


        return (

                <View style={styles.container}>
                  <View style={styles.content}>
                        <Text  style={styles.text}>{name}</Text>
                        <Image resizeMode="cover" style={styles.image} source={image && {uri: image}}/>
                        </View>
                    <View style={styles.detail}>
                <Text><Text numberOfLines={1} style={styles.description}>Abilities :</Text><Text style={styles.descriptionTxt}>  {abilities}</Text></Text>
                  <Text><Text numberOfLines={1} style={styles.description}>Weakness :</Text><Text style={styles.descriptionTxt}> {weakness}</Text></Text>
                  <Text><Text numberOfLines={1} style={styles.description}>weight : </Text><Text style={styles.descriptionTxt}> {weight}</Text></Text>
                  <Text><Text numberOfLines={1} style={styles.description}>Height : </Text><Text style={styles.descriptionTxt}>{height}</Text></Text>
                  <Text><Text numberOfLines={1} style={styles.description}>distance : </Text><Text style={styles.descriptionTxt}>{distance}</Text></Text>

                </View>
                </View>
        )

}
export default Pokemondetail;

const styles = StyleSheet.create({

    container: {
      backgroundColor:"#FF5E3A",
      width: width ,
      height: height ,
      justifyContent:"center",
      alignItems:"center",
      flex:1,
      position:"absolute"

    },
    detail:{
      paddingLeft:10,
      flex:1,
      justifyContent:"flex-start",
      alignItems:"flex-start"
    },
    content:{
      top:10,
      flex:1,
      justifyContent:"center",
      alignItems:"center",
    },
    text:{
      color:"#000",
      fontSize:18,
      marginBottom:15,
      marginTop:15,
      textAlign:'center'
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        color:'#666'
    },
    image:{
      width:200,
      height:200,
      backgroundColor:"#FFF",
      zIndex:-1,
      padding:10,
      borderWidth:2,
      borderColor:"#eee"
    },
    description: {
        fontSize: 12,
        color: '#000',
        flexWrap: 'wrap'
    },
    descriptionTxt: {
        fontSize: 12,
        color: '#666',
        flexWrap: 'wrap'
    },


})
