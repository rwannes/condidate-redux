import React from "react";
import {
    View,
    Dimensions,
    Image,
    Platform,
    StatusBar,
    Animated,
    StyleSheet,
    PermissionsAndroid,
    ScrollView,TouchableOpacity,Modal,Text,Button
} from "react-native"
import { List, ListItem} from 'react-native-elements'
import MapView, {Marker} from 'react-native-maps'
import Interactable from 'react-native-interactable'
import PokemonItem from './pokemonItem'
import Pokemondetail from './Pokemondetail'


const {width, height} = Dimensions.get('window');
const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.0100;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

const GEOLOCATION_OPTIONS = {
    enableHighAccuracy: Platform.OS === 'ios',
    timeout: 1000,
    distanceFilter: 20
}
class Home extends React.Component{
  constructor(props) {
      super(props)
      this._deltaY = new Animated.Value(height - 100);
      this.state = {

          modalVisible: false,
          pokemonSelected:{}
      }
  }
  componentDidMount(){
    var rx=this;
    this.props.getCurrentLocation();
    setTimeout(function(){
    rx.props.getListPokemon();
  },1000);
  }

  detailPokemon(pokemon, visible){

    console.log(pokemon);
    this.setState({modalVisible: visible});
      this.setState({pokemonSelected: pokemon});

  }
  closeModal(visible){
    this.setState({modalVisible: visible});
  }
  render(){

    return (
      <View style={styles.container}>
          <MapView
              style={styles.map}
              showsUserLocation={true}
              followsUserLocation={true}
              userLocationAnnotationTitle={''}
              showsPointsOfInterest={false}
              showsCompass={false}
              showsBuildings={true}
              pitchEnabled={false}
              rotateEnabled={false}
              scrollEnabled={false}
              moveOnMarkerPress={false}
              toolbarEnabled={false}
              zoomEnabled={false}
              zoomControlEnabled={false}
              region={this.props.region}>

              <MapView.Marker
              coordinate={{latitude:this.props.region.latitude, longitude:this.props.region.longitude}}
              pinColor="blue"
              />
              { this.props.listPokemon && this.props.listPokemon.map((pokemon, index)=>
                  <MapView.Marker
                  key= {index}
                  coordinate={{latitude:pokemon.latitude, longitude: pokemon.longitude}}
                  image={pokemon.ThumbnailImage} onPress={() => this.detailPokemon(pokemon, true)}
                  />
                )
              }
          </MapView>

          <View style={styles.panelContainer} pointerEvents={'box-none'}>
              <Animated.View
                  pointerEvents={'box-none'}
                  style={[styles.panelContainer, {
                      backgroundColor: 'black',
                      opacity: 0,
                  }]}/>
              <Interactable.View
                  verticalOnly={true}
                  snapPoints={[{y: 70}, {y: height - 300}, {y: height - 100}]}
                  boundaries={{top: -300}}
                  initialPosition={{y: height - 200}}
                  animatedValueY={this._deltaY}>
                  <View style={styles.panel}>
                      <View style={styles.panelHandle}/>
                      <ScrollView horizontal={false} style={{width: width - 40}}>
                      <List>
                {
                  this.props.listPokemon.map((pokemon, i) => (
                    <TouchableOpacity  key={pokemon.id} style={styles.item} onPress={() => this.detailPokemon(pokemon, true)}>
                <PokemonItem  name={pokemon.name} image={pokemon.ThumbnailImage}  abilities={pokemon.abilities} weakness={pokemon.weakness} distance={pokemon.distance}/>
                    </TouchableOpacity>
                  ))
                  }
                  </List>

                      </ScrollView>
                  </View>
              </Interactable.View>
          </View>
          <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                      console.log('Modal has been closed.');
                    }}>





                  <Button style={styles.closeBtn}
                    onPress={() => {
                      this.closeModal(!this.state.modalVisible);
                    }}
                    title="X" color="#999" />
                    <View >

                      <Pokemondetail name={this.state.pokemonSelected.name}
                                    image={this.state.pokemonSelected.ThumbnailImage}
                                    abilities={this.state.pokemonSelected.abilities}
                                    types={this.state.pokemonSelected.types}
                                    weight={this.state.pokemonSelected.weight}
                                    height={this.state.pokemonSelected.height}
                                    weakness={this.state.pokemonSelected.weakness}
                                    distance={this.state.pokemonSelected.distance}/>



                    </View>
                  </Modal>
            </View>
    );

  }
}
export default Home;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#CCC',
        ...StyleSheet.absoluteFillObject,
    },
    panelContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 3
    },
    panel: {
        height: height + 300,
        padding: 20,
        backgroundColor: 'white',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        shadowColor: '#000000',
        shadowOffset: {width: 0, height: 0},
        shadowRadius: 5,
        shadowOpacity: 0.4,
        height: '100%',
        flexDirection: 'column',
        alignItems: "center"

    },
    panelHeader: {
        alignItems: 'center',
        width: "100%",
        justifyContent: "space-between",
        flexDirection: 'row'
    },
    panelHandle: {
        width: 40,
        height: 8,
        borderRadius: 4,
        backgroundColor: '#00000040',
        marginBottom: 10
    },
    leftHandle: {
        width: width * (1 / 2) - 20,
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: "flex-start",
        alignItems: 'flex-start'
    },
    pickerLeft: {
        width: width * (2 / 8),
        backgroundColor: "#CCF5F5",
        height: 20,
    },
    pickeright: {
        width: width * (3 / 8) - 20,
        backgroundColor: "#CCC",
        height: 20,
    },
    filterText: {
        fontSize: 12,
        color: 'black',
        width: width * (1 / 8),
    },
    rightHandle: {
        width: width * (1 / 2) - 20,
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: "flex-end",
        alignItems: 'flex-end'
    },
    panelTitle: {
        fontSize: 27,
        height: 35
    },
    panelSubtitle: {
        fontSize: 14,
        color: 'gray',
        height: 30,
        marginBottom: 10
    },
    panelButton: {
        padding: 20,
        borderRadius: 10,
        backgroundColor: '#318bfb',
        alignItems: 'center',
        marginVertical: 10
    },
    panelButtonTitle: {
        fontSize: 17,
        fontWeight: 'bold',
        color: 'white'
    },
    map: {
        height: height,
        width: width,
        ...StyleSheet.absoluteFillObject,
    },
    item: {
        borderBottomWidth: 1,
        borderBottomColor: 'lightgrey',
        backgroundColor: 'white',
        padding: 10,
        borderLeftWidth: 4
    },
    closeBtn:{
      height:40,
       borderRadius:20,
       borderWidth:1,
       borderColor:"#999",
       color:"#999",
       top:0,
       position:"absolute",
       width:width,
       backgroundColor:"#FFF",
       flexDirection:'row',
       justifyContent: 'flex-end',
       alignItems: 'flex-end',
         zIndex: 5

    }

});
