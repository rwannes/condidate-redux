import React, {Component} from 'react'
import {Image, StyleSheet, Text, View} from 'react-native'
import IconEntypo from 'react-native-vector-icons/Entypo'

export const PokemonItem =({name, image, abilities, weakness, distance}) => {


        return (

                <View style={styles.itemHeader}>
                    <View style={styles.image}>
                        <Image resizeMode="cover" style={styles.image} source={image && {uri: image}}/>
                    </View>
                    <View style={styles.itemInformation}>
                        <Text numberOfLines={1} style={styles.title}>{name}</Text>
                        <Text numberOfLines={1} style={styles.description}>Abilities :  {abilities}</Text>
                    <Text numberOfLines={1} style={styles.description}>Weakness : {weakness}</Text>
                    </View>
                    <View style={styles.more}>
                        <IconEntypo name="direction" size={12} color="#CCC"/>
                        <Text style={[styles.textItemMore, {color: '#CCC'}]}> {distance} m</Text>
                    </View>
                </View>

        )

}
export default PokemonItem;

const styles = StyleSheet.create({

    itemHeader: {
        flex: 1,
        flexDirection: 'row'
    },
    image: {
        backgroundColor: 'white',
        borderRadius: 10,
        width : 60,
        height : 60,
        zIndex:-1
    },
    itemInformation: {
        marginLeft: 10,
        flex: 1,
        justifyContent: 'center',
        marginTop: -2,
        paddingRight: 10
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    description: {
        fontSize: 12,
        color: '#666',
        flexWrap: 'wrap'
    },
    icon: {
        alignSelf: 'flex-start'
    },
    timelineContainer: {
        marginTop: 10,
        marginBottom: 10,
        height: 2,
        width: '100%',
        backgroundColor: 'lightgrey',
        borderRadius: 4
    },
    timelineProgress: {
        height: 2,
        borderRadius: 4
    },
    itemMore: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    more: {
        flexDirection: 'row',
        alignItems: 'baseline'
    },
    textItemMore: {
        fontSize: 12,
        marginLeft: 5
    }
})
